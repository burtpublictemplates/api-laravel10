<?php

namespace App\Http\Controllers;
use App\Enums\FileName;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Support\Facades\Auth;
use App\Enums\SystemMessage;
use App\Enums\SystemData;
use App\Enums\HttpStatusCode;
use App\Models\User;
use Illuminate\Database\QueryException;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\TokenRepository;

class AuthController extends Controller
{
    public function login(LoginUserRequest $request)
    {

        $loginCredentials=[
            'email' =>$request->email,
            'password' =>$request->password,
        ];

        try{

            if(!Auth::attempt($loginCredentials) ){
                return response()->json([
                    SystemData::Message=>SystemMessage::AuthLoginFailed,
                    SystemData::Success=>false],
                    HttpStatusCode::ClientErrorNotFound);
                }

                $user=User::find(Auth::id());
                $accessToken  =   $user->createToken('Personal Access Token')->accessToken;

                //show found record as object
            return response()->json([
                SystemData::Data=>UserResource::make($user),
                SystemData::AccessToken=> $accessToken,
                SystemData::Message=>SystemMessage::AuthLoginSuccess,
                SystemData::Success=>true]);


            }catch(QueryException $e){ // Catch all Query Errors
                return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Exception $e){     // Catch all General Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Error $e){          // Catch all Php Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }

    }

    public function logout(Request $request){

        //Get the tokenId use getTokenId function from AuthHelper.php in Helpers folder.
        $tokenId = getTokenId($request);

        try{
            // dd('hello');

            //Use the Token Repository to Revoke the Access Token
            $tokenRepository = app(TokenRepository::class);
            $tokenRepository->revokeAccessToken($tokenId);

            return response()->json([
            SystemData::Message=>SystemMessage::AuthLogout,
            SystemData::Success=>true]);

        }catch(QueryException $e){ // Catch all Query Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Exception $e){     // Catch all General Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Error $e){          // Catch all Php Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    public function checkTokenStatus(Request $request){

        try{

            //Get the tokenId use getTokenId function from AuthHelper.php in Helpers folder.
            if(strpos($request->header('authorization'),"Bearer")!==false){
                $tokenId = getTokenId($request);
            }else
            {
                $tokenId=0;
            }

            //check if token still good in the database
            $tokenStatus=DB::table('oauth_access_tokens')->where('id','=',$tokenId)
                                                                ->where('revoked','=',0)->first();

            if(is_null($tokenStatus)){
                return response()->json([
                    SystemData::TokenStatus=>0, // 0 means no longer valid
                    SystemData::Message=>SystemMessage::TokenNoLongerValid,
                    SystemData::Success=>false]);
            }

            return response()->json([
                SystemData::TokenStatus=>1, // 0 means no longer valid
                SystemData::Message=>SystemMessage::TokenStillValid,
                SystemData::Success=>true]);

        }catch(QueryException $e){ // Catch all Query Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Exception $e){     // Catch all General Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Error $e){          // Catch all Php Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }
    }
}
