<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        return $request->expectsJson() ? null : route('unauthorize_route');
    }

    // Add new method
    protected function unauthenticated($request, array $guards)
    {
        abort(response()->json([
            'message'   => SystemMessage::UnauthorizedAccess,
            'success'   => false,
        ],
        HttpStatusCode::ClientErrorUnauthorized));
    }
}
