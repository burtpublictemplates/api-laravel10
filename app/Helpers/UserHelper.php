<?php

use App\Models\User;

if (!function_exists('CheckIfUserEmailExist')) {
    /**
     * description
     *
     * @param
     * @return
     */
    function checkIfUserEmailExist($email)
    {
        $user=User::select(['email'])
        ->where('users.email','=',$email)
        ->get();

        return $user->count();
    }
}

?>
