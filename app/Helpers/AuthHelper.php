<?php

use Illuminate\Http\Request;

if (!function_exists('burtTest2')) {
    function burtTest2(){
        echo "Hello this is just a helper test created by burt";
    }

}

function getTokenId(Request $request)
{
     //Extract First the TokenID from the $request
     $access_token = $request->header('Authorization');
     $auth_header = explode(' ', $access_token);
     $token = $auth_header[1];
     $token_parts = explode('.', $token);
     $token_header = $token_parts[1];
     $token_header_json = base64_decode($token_header);
     $token_header_array = json_decode($token_header_json, true);

     //Get the tokenID
     $tokenId = $token_header_array['jti'];

     return $tokenId;
}

?>
