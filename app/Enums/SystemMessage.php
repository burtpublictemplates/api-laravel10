<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SystemMessage extends Enum
{
    const InvalidCredentials = "Authentication Fail. Invalid Credentials";
    const AuthLoginFailed="Authentication Failed. Invalid credentials";
    const AuthLoginSuccess="Authentication Successful.";
    const ApplicationError = "Application Error";
    const GeneralError = "General Error";
    const QueryError = "Query Error";
    const ValidationError="Validation Errors";

    //File Messages
    const FileFormatNotSupported = "File format not supported";

     //Common Messages
    const RejectionDetails="N1 transaction(s) successfully added But failed to add the following past transactions. Click Add button to add them one by one.";
    const NotFound=" Not Found";
    const AlreadyBeenUsed = " as it is already been used as referrence by other records";
    const StatsRetrieved = "Stats has been retrieved";
    const SystemLoginUrl = "https://beta-igo.talentbuilders.org/login";
    const FileReadyForDownload="File is ready for download";
    const FileDownloadInitiated="File download initiated";
    const AuthLogout="User has been logout. Goodbye.";
    const TokenNoLongerValid = "Token no longer valid";
    const TokenStillValid = "Token is still valid";
    const UnauthorizedAccess = "Unauthorize access.You don't have permission to access this route.";


    //User Messages
    const UserRecordCreated="New user record has been created";
    const UserRecordUpdated="User record has been updated";
    const UserRecordDeleted="User record has been deleted";
    const UserRecordRestored="User record has been restored";
    const UserRecordFound="User record found";
    const UserRecordRetrieved="User record has been retrieved";
    const UserNoRecordFound="No user record found";
    const UserID="User id ";
    const UserCanNotDelete="Can not delete user id ";
    const UserProfilePicUpdated="User Profile Pic has been updated.";
    const UserEmail="User Email ";
    const UserEmailAlreadyExist="Unable to create user record. Email already exist. Please use another email.";

    //Mail Messages
    const MailSubjectAccessCredential="Your iGo Access Credential";
    const MailSubjectResetPassword="You Have Requested For Password Reset";
    const MailResetPasswordMailed="A reset password verification code has been sent to your email. Check your email get the verification code and complete your password reset. Thank you.";


}
