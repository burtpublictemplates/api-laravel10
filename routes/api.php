<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {

    //Unprotected Routes *********************************************************************//

    Route::post('auth/login',[AuthController::class,'login']);

    //Forget Password and Password Reset Routes
    Route::post('auth/forgot-password',[AuthController::class,'forgotPassword']);
    Route::post('auth/reset-password',[AuthController::class,'resetPassword']);

    //Protected Routes *************************************************************************//

    //Authentication Route
    Route::middleware('auth:api')->prefix('auth')->group(function () {
        Route::middleware('auth:api')->post('/logout',[AuthController::class,'logout']);
        Route::middleware('auth:api')->post('check-token-status',[AuthController::class,'checkTokenStatus']);
    });

    //User Route
     Route::middleware('auth:api')->prefix('user')->group(function () {
        Route::match(['get', 'post'], '', [UserController::class,'index']);
        Route::match(['get', 'post'],'/show/{id}',[UserController::class,'show']);
        Route::match(['get', 'post'], '/store', [UserController::class,'store']);
        Route::match(['get', 'post', 'put'], '/update/{id}', [UserController::class,'update']);
        Route::match(['get', 'delete'], '/delete/{id}', [UserController::class,'destroy']);
        Route::match(['get', 'post'], '/restore/{id}', [UserController::class,'undestroy']);
        Route::match(['get', 'post'], '/stats', [UserController::class,'stats']);
        });


});
